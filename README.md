# nginx

Install and configure the webservice nginx.

## Requirements

none

## Role Variables

none

## Dependencies
------------

none

## Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: cinux.nginx }

## License
-------

MIT
