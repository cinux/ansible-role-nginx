# frozen_string_literal: true
# Molecule managed

[
    "nginx"
].each do |pkg|
    describe package(pkg) do
        it { should be_installed }
    end
end

describe service('nginx') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
end

describe port(80) do
    it { should be_listening }
end

describe command('curl -I localhost') do
    its('stdout') { should match ('HTTP/1.1 200 OK') }
end