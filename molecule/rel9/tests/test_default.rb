# frozen_string_literal: true
# Molecule managed

[
    "nginx"
].each do |pkg|
    describe package(pkg) do
        it { should be_installed }
    end
end

s = [
    '# role: nginx',
    '# file: templates/nginx.repo.j2',
    '[nginx]',
    'name=nginx repo',
    'baseurl=http://nginx.org/packages/centos/9/$basearch/',
    'gpgcheck=1',
    'gpgkey=https://nginx.org/keys/nginx_signing.key',
    'enabled=1'
].join("\n")
describe file('/etc/yum.repos.d/nginx.repo') do
    its('owner') { should eq 'root'}
    its('group') { should eq 'root'}
    its('mode') { should cmp '0644'}
    its('content') { should match(s) }
end

describe service('nginx') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
end

describe port(80) do
    it { should be_listening }
end

describe command('curl -I localhost') do
    its('stdout') { should match ('HTTP/1.1 200 OK') }
end